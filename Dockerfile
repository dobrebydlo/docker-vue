ARG NODE_IMAGE="dobrebydlo/node:22-1.0.0"

FROM ${NODE_IMAGE}

ARG VUE_VERSION="3.5.13"

ENV VUE_VERSION=${VUE_VERSION}

USER root

RUN npm install --location=global \
    vue@${VUE_VERSION} \
    @vue/cli-service \
    @vue/cli-plugin-babel \
    @vue/cli-plugin-eslint \
    @vue/cli-plugin-typescript \
    @vue/cli-plugin-unit-jest \
    @vue/cli-plugin-router \
    @vue/cli-plugin-vuex \
    webpack

USER ${UNAME}
